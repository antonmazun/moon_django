from django.db import models


# Create your models here.


class People(models.Model):
    name = models.CharField(max_length=255, verbose_name='Name')
    surname = models.CharField(max_length=255, verbose_name='Surname')
    age = models.IntegerField(verbose_name='age')

    def __str__(self):
        return 'Name is {} surname is {} age is {}'.format(self.name, self.surname, self.age)
